import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as _ from 'underscore';

@Component({
  selector: 'app-work-paging',
  templateUrl: './work-paging.component.html',
  styleUrls: ['./work-paging.component.css']
})
export class WorkPagingComponent implements OnInit {

 constructor() { }

  ngOnInit() {
  }
  
  public persons:Array<any> = [
    {"name" : "eBdesk Malaysia"
    },
    {"name" : "Metro"
    },
    {"name" : "Partner"
    },
    {"name" : "Test23022017-3"
    },
    { "name" :   "General"
    },
    { "name" : "NLP eBdesk"
    },
    {"name" : "Test23022017-2"
    },
    {"name" : "Test23022017-4"
    },
   ]
   
}
