import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkPagingComponent } from './src/work-paging/work-paging.component';

export * from './src/work-paging/work-paging.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    
    WorkPagingComponent
  ],
  exports: [
    
    WorkPagingComponent
  ]
})
export class WorkPaging {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WorkPaging,
      providers: []
    };
  }
}
